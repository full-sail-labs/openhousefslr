using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car_Controller_RPM : MonoBehaviour
{
    [Header("Component Assignments")]
    //Car Rigid Body
    [SerializeField] Rigidbody CarBody;
    //Wheel Colliders
    [SerializeField] WheelCollider FrontRight;
    [SerializeField] WheelCollider FrontLeft;
    [SerializeField] WheelCollider RearRight;
    [SerializeField] WheelCollider RearLeft;
    //Wheel Meshes
    [SerializeField] Transform M_FrontRight;
    [SerializeField] Transform M_FrontLeft;
    [SerializeField] Transform M_RearRight;
    [SerializeField] Transform M_RearLeft;

    //Public Variable that controll the overall power of the engine
    [Header("Power Values")]
    public float MaxEngineRPM = 5000.0f;
    public float HorsePower = 600.0f;
    public float MaxTorque = 800.0f;
    public float MinRPM = 1000.0f;
    public float DownForce = 10.0f;

    [Header("Driving Values")]
    public float MaxSteerAngle = 30.0f;
    public float BrakeStrength = 5000;
    public float HandBrakeStrength = 5000;

    [Header("RPM Ratios")]
    public float G1_RpmRatio = 0.2f;
    public float G2_RpmRatio = 0.4f;
    public float G3_RpmRatio = 0.6f;
    public float G4_RpmRatio = 0.8f;
    public float G5_RpmRatio = 0.93f;
    public float G6_RpmRatio = 1.1f;
    public float GR_RpmRatio = 100f;

    //Declaring enums for different types of transmission systems
    public enum TransmissionType { FWD, RWD, AWD};
    public TransmissionType CarTransmission = TransmissionType.FWD;

    //Private Gear Enum variable
    private enum GearCount { Gear1, Gear2, Gear3, Gear4, Gear5, Gear6, Reverse};
    private GearCount CurrentGear;

    //The maximum RPM the car can go to based on the current gear
    private float CurrentMaxRPM = 0.0f;
    private float CurrentRPMRatio = 0.4f;

    //Variable to check if the throttle brakes or handbrakes are on/off
    private bool Throttle = false;
    private bool Braking = false;
    private bool Handbraking = false;
    private bool Reverse = false;
    
    //The current RPM the car is at
    private float CurrentRPM = 1000.0f;

    // Start is called before the first frame update
    void Start()
    {
        // Setting the first gear to 1
        CurrentGear = GearCount.Gear1;

        //Make Sure torque is not more than 5000
        if (MaxTorque > 5000)
        {
            MaxTorque = 5000.0f;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    //Called 50 Times a second
    private void FixedUpdate()
    {
        //Update Inputs toggles booleans depending upon which input keys are pressed. 
        //It checks for Throttle, Brake, Handbrake and Reverse
        UpdateInputs();

        //SetMaxRPM that can be acheived in the current Gear. Game always starts at 1st gear and gears change automatically
        SetMaxGearRPM(CurrentGear);

        //Incrememnt RPM 
        UpdateRPM(CurrentMaxRPM);

        //Apply Torque based on the incrementing RPM 
        ApplyTorque(CarTransmission);

        //Apply the brakes
        UpdateBrakes();

        //Update Steering
        FrontLeft.steerAngle = MaxSteerAngle * Input.GetAxis("Horizontal");
        FrontRight.steerAngle = MaxSteerAngle * Input.GetAxis("Horizontal");

        //Update Wheel meshes
        UpdateWheel(FrontRight, M_FrontRight);
        UpdateWheel(FrontLeft, M_FrontLeft);
        UpdateWheel(RearRight, M_RearRight);
        UpdateWheel(RearLeft, M_RearLeft);

        Debug.Log("Throttle = " + Throttle + " --- Brake = " + Braking + " --- Handbrake = " + Handbraking + "  ----  Reverse = " + Reverse + " --- Current Velocity = " + (CarBody.velocity.magnitude*(36/10)*2));
    }

    //Function to apply torque on wheels> dependent upon current RPM and transmission
    void ApplyTorque(TransmissionType l_transmission)
    {
        if (Throttle == true)
        {
            float AppliedTorque = (CurrentRPM / MaxEngineRPM) * MaxTorque + (20 / CurrentRPMRatio);

            if(!Reverse)
            {
                switch (l_transmission)
                {
                    case TransmissionType.FWD:
                        if (FrontRight.motorTorque <= ((CurrentMaxRPM / MaxEngineRPM) * MaxTorque))
                        {
                            FrontRight.motorTorque = AppliedTorque / 2;
                            FrontLeft.motorTorque = AppliedTorque / 2;
                        }
                        break;

                    case TransmissionType.RWD:
                        if (RearRight.motorTorque <= ((CurrentMaxRPM / MaxEngineRPM) * MaxTorque))
                        {
                            RearRight.motorTorque = AppliedTorque / 2;
                            RearLeft.motorTorque = AppliedTorque / 2;
                        }
                        break;

                    case TransmissionType.AWD:
                        if (FrontRight.motorTorque <= ((CurrentMaxRPM / MaxEngineRPM) * MaxTorque))
                        {
                            FrontRight.motorTorque = AppliedTorque * 0.8f;
                            FrontLeft.motorTorque = AppliedTorque * 0.8f;
                            RearRight.motorTorque = AppliedTorque * 0.8f;
                            RearLeft.motorTorque = AppliedTorque * 0.8f;
                        }
                        break;
                }

                
                return;
            }

            if(Reverse)
            {
                switch (l_transmission)
                {
                    case TransmissionType.FWD:
                        if (FrontRight.motorTorque >= ((CurrentMaxRPM / MaxEngineRPM) * MaxTorque))
                        {
                            FrontRight.motorTorque = AppliedTorque;
                            FrontLeft.motorTorque = AppliedTorque;
                        }
                        break;

                    case TransmissionType.RWD:
                        if (RearRight.motorTorque >= ((CurrentMaxRPM / MaxEngineRPM) * MaxTorque))
                        {
                            RearRight.motorTorque = AppliedTorque;
                            RearLeft.motorTorque = AppliedTorque;
                        }
                        break;

                    case TransmissionType.AWD:
                        if (FrontRight.motorTorque >= ((CurrentMaxRPM / MaxEngineRPM) * MaxTorque))
                        {
                            FrontRight.motorTorque = AppliedTorque;
                            FrontLeft.motorTorque = AppliedTorque;
                            RearRight.motorTorque = AppliedTorque;
                            RearLeft.motorTorque = AppliedTorque;
                        }
                        break;
                }
                return;
            } 
        }

        //Setting the applied torque to 0 when the accelerator is not being pressed. This nullfies any input given to the car through the wheels
        if (Throttle == false)
        {
            FrontRight.motorTorque = 0;
            FrontLeft.motorTorque = 0;
            RearRight.motorTorque = 0;
            RearLeft.motorTorque = 0;
        }

        //Setting the applied torque to 0 When the accelerator is not being pressed and the car is close to being stopped
        if (CarBody.velocity.magnitude < .1 && Throttle == false)
        {
            FrontRight.motorTorque = 0;
            FrontLeft.motorTorque = 0;
            RearRight.motorTorque = 0;
            RearLeft.motorTorque = 0;
        }

        //Apply Downforce
        CarBody.AddForce(transform.up * -1* DownForce);
        
    }

    //Function to increment RPM to the current maximum
    void UpdateRPM(float l_CurrentMaxRPM)
    {
        
        //Executed When reverse is false
        if(!Reverse)
        {
            //Incrementing RPM when Throttle is pressed
            if (Throttle == true)
            {
                //Incrememnt
                if (CurrentRPM < l_CurrentMaxRPM && CurrentRPM < CurrentMaxRPM)
                {
                    CurrentRPM = CurrentRPM + ((HorsePower / 300) + (1 / CurrentRPMRatio));
                }

                //Incrementing current gear to next when the respective RPM count is exceeded
                if (CurrentRPM >= l_CurrentMaxRPM && CurrentGear != GearCount.Gear6)
                {
                    IncrementGear(CurrentGear);
                }
            }
            
            //Making sure RPM approaches the value of Minimum RPM when the throttle is not pressed
            if (Throttle == false && CurrentRPM > 1000)
            {
                CurrentRPM = CurrentRPM - 1 * (HorsePower) / 50;

                //Decrement gears when the RPM drops below a specific RPM ratio
                if (CurrentRPM < l_CurrentMaxRPM && CurrentGear != GearCount.Gear1)
                {
                    DecrementGear(CurrentGear);
                }
            }

            //Making sure RPM value never goes below minimum
            if (CurrentRPM < MinRPM)
            {
                CurrentRPM = MinRPM;
            }

            return;
        }

        //Executed when reverse is True
        else if (Reverse)
        {
            //If throttle is presesd, decrement RPM numbers
            if (Throttle == true)
            {
                if (CurrentRPM > l_CurrentMaxRPM)
                {
                    CurrentRPM = (CurrentRPM - (HorsePower / 300) - (CurrentRPMRatio));
                }
            }
            
            //Making sure RPM approaches 0 when trottle is not pressed
            if (Throttle == false && CurrentRPM < 0)
            {
                Debug.Log("Trying to set the RPM to zero from a negative value");
                CurrentRPM = CurrentRPM + 1 * (HorsePower) / 50;
            }

            //Making sure Engine RPM dureing reverse never goes above 0
            if (CurrentRPM > MinRPM)
            {
                CurrentRPM = MinRPM;
            }

            return;
        }

        //Slowly reducing the EngineRPM when throttle is not pressed
       
        //making sure engine rpm never goes below minimum
       
    }

    //Function to change the gear to the next one
    void IncrementGear(GearCount l_currentgear)
    {
        switch (l_currentgear)
        {
            case GearCount.Gear1:
                CurrentGear = GearCount.Gear2;
                CurrentRPMRatio = G2_RpmRatio;
                SetMaxGearRPM(CurrentGear);
                Debug.LogWarning("Gear Set tp 2");
                break;

            case GearCount.Gear2:
                CurrentGear = GearCount.Gear3;
                CurrentRPMRatio = G3_RpmRatio;
                SetMaxGearRPM(CurrentGear);
                Debug.LogWarning("Gear Set tp 3");
                break;

            case GearCount.Gear3:
                CurrentGear = GearCount.Gear4;
                CurrentRPMRatio = G4_RpmRatio;
                SetMaxGearRPM(CurrentGear);
                Debug.LogWarning("Gear Set tp 4");
                break;

            case GearCount.Gear4:
                CurrentGear = GearCount.Gear5;
                CurrentRPMRatio = G5_RpmRatio;
                SetMaxGearRPM(CurrentGear);
                Debug.LogWarning("Gear Set tp 5");
                break;

            case GearCount.Gear5:
                CurrentGear = GearCount.Gear6;
                CurrentRPMRatio = G6_RpmRatio;
                SetMaxGearRPM(CurrentGear);
                Debug.LogWarning("Gear Set tp 6");
                break;

            case GearCount.Gear6:
                break;
        }
    }

    //Function to bring gearcount down
    void DecrementGear(GearCount l_currentgear)
    {
        switch (l_currentgear)
        {
            case GearCount.Gear1:
                Debug.LogWarning("Cannot Decrement because smallest gear");
                break;

            case GearCount.Gear2:
                if (CurrentRPM < CurrentMaxRPM)
                {
                    CurrentGear = GearCount.Gear1;
                    Debug.LogWarning("Gear Decremented Successfully. Gear Set to : " + CurrentGear);
                }
                break;

            case GearCount.Gear3:
                if (CurrentRPM < CurrentMaxRPM)
                {
                    CurrentGear = GearCount.Gear2;
                    Debug.LogWarning("Gear Decremented Successfully. Gear Set to : " + CurrentGear);
                }
                break;

            case GearCount.Gear4:
                if (CurrentRPM < CurrentMaxRPM)
                {
                    CurrentGear = GearCount.Gear3;
                    Debug.LogWarning("Gear Decremented Successfully. Gear Set to : " + CurrentGear);
                }
                break;

            case GearCount.Gear5:
                if (CurrentRPM < CurrentMaxRPM)
                {
                    CurrentGear = GearCount.Gear4;
                    Debug.LogWarning("Gear Decremented Successfully. Gear Set to : " + CurrentGear);
                }
                break;

            case GearCount.Gear6:
                if (CurrentRPM < CurrentMaxRPM)
                {
                    CurrentGear = GearCount.Gear5;
                    Debug.LogWarning("Gear Decremented Successfully. Gear Set to : " + CurrentGear);
                }
                break;
        }
    }

    //Fucntion to generate values for Max RPM for the current gear the car is in //Called Once in the beginning
    void SetMaxGearRPM(GearCount l_currentgear)
    {
        switch (l_currentgear)
        {
            case GearCount.Gear1:
                CurrentMaxRPM = (float)(MaxEngineRPM * G1_RpmRatio);
                break;

            case GearCount.Gear2:
                CurrentMaxRPM = (float)(MaxEngineRPM * G2_RpmRatio);
                break;

            case GearCount.Gear3:
                CurrentMaxRPM = (float)(MaxEngineRPM * G3_RpmRatio);
                break;

            case GearCount.Gear4:
                CurrentMaxRPM = (float)(MaxEngineRPM * G4_RpmRatio);
                break;

            case GearCount.Gear5:
                CurrentMaxRPM = (float)(MaxEngineRPM * G5_RpmRatio);
                break;

            case GearCount.Gear6:
                CurrentMaxRPM = (float)(MaxEngineRPM * G6_RpmRatio);
                break;

            case GearCount.Reverse:
                CurrentMaxRPM = -0.8f * MaxEngineRPM;
                break;

            default:
                CurrentMaxRPM = (float)(MaxEngineRPM * G1_RpmRatio);
                break;
        }
    }

    //Funciton to Update the Input and change a few boolean variable that control driving
    void UpdateInputs()
    {
        //Setting throttle to True if the button is being pressed
        if(Input.GetKey(KeyCode.UpArrow))
        {
            Throttle = true;
        }
        else
        {
            Throttle = false;
        }

        //Setting Braking to true if down arrow is pressed
        if(Input.GetKey(KeyCode.DownArrow))
        {
            Braking = true;
        }
        else
        {
            Braking = false;
        }

        //Setting Handbrake to true if Spacebar is pressed
        if(Input.GetKey(KeyCode.Space))
        {
            Handbraking = true;
        }
        else
        {
            Handbraking = false;
        }

        if(Input.GetKeyDown(KeyCode.R))
        {
            EngageReverse();
        }
    }


    //This function attempts to puts the Car into reverse gear 
    void EngageReverse()
    {
        //ENGAGES the reverse Gear
        if(CarBody.velocity.magnitude < 1 && Reverse == false)
        {
            Reverse = true;
            CurrentGear = GearCount.Reverse;
            SetMaxGearRPM(CurrentGear);
            CurrentRPM = -1;
            Debug.LogWarning("Reverse Engaged");
            return;
        }

        //DISENGAGES the reverse gear
        else if(CarBody.velocity.magnitude < 1 && Reverse == true)
        {
            Reverse = false;
            CurrentGear = GearCount.Gear1;
            SetMaxGearRPM(CurrentGear);
            CurrentRPM = 1000;
            Debug.LogWarning("Reverse Disengaged");
            return;
        }
    }

    //Funtion to Apply Brakes
    void UpdateBrakes()
    {
        if (Braking == true)
        {
            FrontLeft.brakeTorque = BrakeStrength;
            FrontRight.brakeTorque = BrakeStrength;
        }
        else
        {
            FrontLeft.brakeTorque = 0;
            FrontRight.brakeTorque = 0;
        }

        if (Handbraking == true)
        {
            RearLeft.brakeTorque = HandBrakeStrength;
            RearRight.brakeTorque = HandBrakeStrength;
        }
        else
        {
            RearLeft.brakeTorque = 0;
            RearRight.brakeTorque = 0;
        }
    }

    //Function to update the wheel meshes
    void UpdateWheel(WheelCollider WCollider, Transform WTransform)
    {
        // get wheel collider state
        Vector3 WheelPosition;
        Quaternion WheelRotation;

        WCollider.GetWorldPose(out WheelPosition, out WheelRotation);

        //Set wheel transform State
        WTransform.position = WheelPosition;
        WTransform.rotation = WheelRotation;
    }
}
