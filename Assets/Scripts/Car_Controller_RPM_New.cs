using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car_Controller_RPM_New : MonoBehaviour
{
    [Header("Component Assignments")]
    //Car Rigid Body
    [SerializeField] Rigidbody CarBody;
    //Wheel Colliders
    [SerializeField] WheelCollider FrontRight;
    [SerializeField] WheelCollider FrontLeft;
    [SerializeField] WheelCollider RearRight;
    [SerializeField] WheelCollider RearLeft;
    //Wheel Meshes
    [SerializeField] Transform M_FrontRight;
    [SerializeField] Transform M_FrontLeft;
    [SerializeField] Transform M_RearRight;
    [SerializeField] Transform M_RearLeft;

    //Public Variable that controll the overall power of the engine
    [Header("Engine Values")]
    public float horsePower = 600.0f;
    public float maxRPM = 8000.0f;
    public float minRPM = 1000.0f;
    public float maxTorque = 5000.0f;

    //Minimum RPM depending upon each gear
    [Header("Gear Min RPMs")]
    public float gear01_MinRPMs = 1000;
    public float gear02_MinRPMs = 4000;
    public float gear03_MinRPMs = 4500;
    public float gear04_MinRPMs = 5500;
    public float gear05_MinRPMs = 6500;
    public float gear06_MinRPMs = 6950;
    public float gearReverse_MinRPMs = 1000;

    [Header("Gear Max RPMs")]
    public float gear01_MaxRPMs = 6000;
    public float gear02_MaxRPMs = 6500;
    public float gear03_MaxRPMs = 6900;
    public float gear04_MaxRPMs = 7000;
    public float gear05_MaxRPMs = 7500;
    public float gear06_MaxRPMs = 8000;
    public float gearReverse_MaxRPMs = 5000;

    //Torque Ratios for each gear. (Might not be necessacry Just for testing)
    [Header("Gear Data")]
    private float gear01_TauRatio = 0.4f;
    private float gear02_TauRatio = 0.45f;
    private float gear03_TauRatio = 0.52f;
    private float gear04_TauRatio = 0.6f;
    private float gear05_TauRatio = 0.65f;
    private float gear06_TauRatio = 0.7f;
    private float gearReverse_TauRatio = 0.5f;

    //variable declarations for Driving Values
    [Header("Driving Values")]
    public float maxSteerAngle = 15.0f;
    public float brakeStrength = 5000;
    public float handBrakeStrength = 10000;
    
    //Private Gear Enum variable
    private enum GearCount { Gear1, Gear2, Gear3, Gear4, Gear5, Gear6, Reverse };

    struct gear
    {
        public GearCount gearID;
        public float gear_MinRPM;
        public float gear_MaxRPM;
        public float gear_TauRatio;
        public float gear_MinVelo;
        public float gear_MaxVelo;
    }

    gear currentGear; // the current gear engaged;
    gear gear01;
    gear gear02;
    gear gear03;
    gear gear04;
    gear gear05;
    gear gear06;
    gear gearReverse;

    //Declaring enums for different types of transmission systems
    public enum TransmissionType { FWD, RWD, AWD};
    public TransmissionType CarTransmission = TransmissionType.FWD;

    //Boolean Variable to for Car Control
    private bool Throttle = false;
    private bool Braking = false;
    private bool Handbraking = false;
    private bool Reverse = false;
    
    //The current RPM the car is at
    private float currentRPM = 0;
    private float currentVelo = 0;

    // Start is called before the first frame update
    void Start()
    {
        //setting the values for the Gear 1 Struct
        gear01.gearID = GearCount.Gear1;
        gear01.gear_MinRPM = gear01_MinRPMs;
        gear01.gear_MaxRPM = gear01_MaxRPMs;
        gear01.gear_TauRatio = gear01_TauRatio;
        gear01.gear_MinVelo = 0f;
        gear01.gear_MaxVelo = 25f;

        //setting the values for the Gear 2 Struct
        gear02.gearID = GearCount.Gear2;
        gear02.gear_MinRPM = gear02_MinRPMs;
        gear02.gear_MaxRPM = gear02_MaxRPMs;
        gear02.gear_TauRatio = gear02_TauRatio;
        gear02.gear_MinVelo = 25f;
        gear02.gear_MaxVelo = 50f;

        //Setting the values for the Gear 3 Struct
        gear03.gearID = GearCount.Gear3;
        gear03.gear_MinRPM = gear03_MinRPMs;
        gear03.gear_MaxRPM = gear03_MaxRPMs;
        gear03.gear_TauRatio = gear03_TauRatio;
        gear03.gear_MinVelo = 50f;
        gear03.gear_MaxVelo = 85f;

        //Setting the values for the Gear 4 Struct
        gear04.gearID = GearCount.Gear4;
        gear04.gear_MinRPM = gear04_MinRPMs;
        gear04.gear_MaxRPM = gear04_MaxRPMs;
        gear04.gear_TauRatio = gear04_TauRatio;
        gear04.gear_MinVelo = 85f;
        gear04.gear_MaxVelo = 110f;

        //Setting the values for the Gear 5 Struct
        gear05.gearID = GearCount.Gear5;
        gear05.gear_MinRPM = gear05_MinRPMs;
        gear05.gear_MaxRPM = gear05_MaxRPMs;
        gear05.gear_TauRatio = gear05_TauRatio;
        gear05.gear_MinVelo = 110f;
        gear05.gear_MaxVelo = 150f;

        //Setting the values for the Gear 6 Struct
        gear06.gearID = GearCount.Gear6;
        gear06.gear_MinRPM = gear06_MinRPMs;
        gear06.gear_MaxRPM = gear06_MaxRPMs;
        gear06.gear_TauRatio = gear06_TauRatio;
        gear06.gear_MinVelo = 150f;
        gear06.gear_MaxVelo = 180f;

        //Setting the values for the Reverse Gear Struct
        gearReverse.gearID = GearCount.Reverse;
        gearReverse.gear_MinRPM = gearReverse_MinRPMs;
        gearReverse.gear_MaxRPM = gearReverse_MaxRPMs;
        gearReverse.gear_TauRatio = gearReverse_TauRatio;

        currentGear = gear01;
        currentRPM = minRPM;
    }
    //Update is called once per frame
    void Update()
    {
        //Update Inputs toggles booleans depending upon which input keys are pressed. 
        //It checks for Throttle, Brake, Handbrake and Reverse
        UpdateInputs();

        //Update RPMs
        //-----------------------------------------------------------------------------------------//

        //Increase Engine RPM when throttle is pressed
        if (Throttle == true)
        {

            currentRPM += horsePower / 35;
            if (currentRPM > currentGear.gear_MaxRPM)
            {
                currentRPM = currentGear.gear_MaxRPM + 20;
            }
            if (currentRPM >= currentGear.gear_MaxRPM && currentVelo >= currentGear.gear_MaxVelo)
            {
                IncrementGear(currentGear);
                return;
            }
        }

        //Constantly reduce RPM if not accelerating
        else if (Throttle == false && currentRPM >= minRPM)
        {
            currentRPM -= 20; //Reduces RPM by 1000 each second
        }

        //decrement gear
        if (currentVelo <= currentGear.gear_MinVelo)
        {
            DecrementGear(currentGear);
            return;
        }
        //--------------------------------------------------------------------------------------//

        //Update Wheel meshes
        UpdateWheel(FrontRight, M_FrontRight);
        UpdateWheel(FrontLeft, M_FrontLeft);
        UpdateWheel(RearRight, M_RearRight);
        UpdateWheel(RearLeft, M_RearLeft);

        Debug.Log("Throttle = " + Throttle + " |||| Brake = " + Braking + " |||| Handbrake = " + Handbraking + "  ||||  Reverse = " + Reverse);
        Debug.Log("Current Gear = " + currentGear.gearID + " |||| Current Minimum RPM = " + currentGear.gear_MinRPM + " |||| Current Maximum RPM = " + currentGear.gear_MaxRPM + " |||| Current RPM = " + currentRPM);
    }

    //Called 50 Times a second
    private void FixedUpdate()
    {
        //Apply Torque based on the incrementing RPM 
        ApplyTorque(CarTransmission);

        //Apply the brakes
        UpdateBrakes();

        //Update Steering
        FrontLeft.steerAngle = maxSteerAngle * Input.GetAxis("Horizontal");
        FrontRight.steerAngle = maxSteerAngle * Input.GetAxis("Horizontal");
    }

    //Function to apply torque on wheels> dependent upon current RPM and transmission
    void ApplyTorque(TransmissionType l_transmission)
    {
        float AppliedTorque = (horsePower * 5252) / (currentRPM * currentGear.gear_TauRatio);

        if(AppliedTorque >= maxTorque)
        {
            AppliedTorque = maxTorque;
        }
        Debug.Log("Current Velocity = " + currentVelo + " |||| Applied Torque = " + AppliedTorque);

        if (Throttle == true)
        {
            if(!Reverse)
            {
                switch (l_transmission)
                {
                    case TransmissionType.FWD:
                            FrontRight.motorTorque = AppliedTorque / 2;
                            FrontLeft.motorTorque = AppliedTorque / 2;
                        break;

                    case TransmissionType.RWD:
                            RearRight.motorTorque = AppliedTorque / 2;
                            RearLeft.motorTorque = AppliedTorque / 2;
                        break;

                    case TransmissionType.AWD:
                            FrontRight.motorTorque = AppliedTorque * 0.2f;
                            FrontLeft.motorTorque = AppliedTorque * 0.2f;
                            RearRight.motorTorque = AppliedTorque * 0.2f;
                            RearLeft.motorTorque = AppliedTorque * 0.2f;
                        break;
                }
                return;
            }

            if(Reverse)
            {
                switch (l_transmission)
                {
                    case TransmissionType.FWD:
                        FrontRight.motorTorque = -AppliedTorque / 3;
                        FrontLeft.motorTorque = -AppliedTorque / 3;
                        break;

                    case TransmissionType.RWD:
                        RearRight.motorTorque = -AppliedTorque / 3;
                        RearLeft.motorTorque = -AppliedTorque / 3;
                        break;

                    case TransmissionType.AWD:
                        FrontRight.motorTorque = -AppliedTorque * 0.1f;
                        FrontLeft.motorTorque = -AppliedTorque * 0.1f;
                        RearRight.motorTorque = -AppliedTorque * 0.1f;
                        RearLeft.motorTorque = -AppliedTorque * 0.1f;
                        break;
                }
                return;
            } 
        }

        //Setting the applied torque to 0 when the accelerator is not being pressed. This nullfies any input given to the car through the wheels
        if (Throttle == false)
        {
            FrontRight.motorTorque = 0;
            FrontLeft.motorTorque = 0;
            RearRight.motorTorque = 0;
            RearLeft.motorTorque = 0;
        }       
    }

    //Function to change the gear to the next one
    void IncrementGear(gear l_currentgear)
    {
        switch (l_currentgear.gearID)
        {
            case GearCount.Gear1:
                currentGear = gear02;
                currentRPM = gear02_MinRPMs;
                Debug.LogWarning("Gear Set tp 2");
                break;

            case GearCount.Gear2:
                currentGear = gear03;
                currentRPM = gear03_MinRPMs;
                Debug.LogWarning("Gear Set tp 3");
                break;

            case GearCount.Gear3:
                currentGear = gear04;
                currentRPM = gear04_MinRPMs;
                Debug.LogWarning("Gear Set tp 4");
                break;

            case GearCount.Gear4:
                currentGear = gear05;
                currentRPM = gear05_MinRPMs;
                Debug.LogWarning("Gear Set tp 5");
                break;

            case GearCount.Gear5:
                currentGear = gear06;
                currentRPM = gear06_MinRPMs;
                Debug.LogWarning("Gear Set tp 6");
                break;

            case GearCount.Gear6:
                break;
        }
    }

    //Function to bring gearcount down
    void DecrementGear(gear l_currentgear)
    {
        switch (l_currentgear.gearID)
        {
            case GearCount.Gear1:
                Debug.LogWarning("Cannot Decrement because smallest gear");
                break;

            case GearCount.Gear2:
                currentGear = gear01;
                currentRPM = gear01_MinRPMs;
                Debug.LogWarning("Gear Decremented Successfully. Gear Set to 1");
                break;

            case GearCount.Gear3:
                currentGear = gear02;
                currentRPM = gear02_MinRPMs;
                Debug.LogWarning("Gear Decremented Successfully. Gear Set to 2");
                break;

            case GearCount.Gear4:
                currentGear = gear03;
                currentRPM = gear03_MinRPMs;
                Debug.LogWarning("Gear Decremented Successfully. Gear Set to 3");
                break;

            case GearCount.Gear5:
                currentGear = gear04;
                currentRPM = gear04_MinRPMs;
                Debug.LogWarning("Gear Decremented Successfully. Gear Set to 4");
                break;

            case GearCount.Gear6:
                currentGear = gear05;
                currentRPM = gear05_MinRPMs;
                Debug.LogWarning("Gear Decremented Successfully. Gear Set to 1");
                break;
        }
    }

    //Funciton to Update the Input and change a few boolean variable that control driving
    void UpdateInputs()
    {
        //Setting throttle to True if the button is being pressed
        if(Input.GetKey(KeyCode.UpArrow))
        {
            Throttle = true;
            CarBody.drag = 0;
        }
        else
        {
            Throttle = false;
            CarBody.drag = 0.3f;
        }

        //Setting Braking to true if down arrow is pressed
        if(Input.GetKey(KeyCode.DownArrow))
        {
            Braking = true;
        }
        else
        {
            Braking = false;
        }

        //Setting Handbrake to true if Spacebar is pressed
        if(Input.GetKey(KeyCode.Space))
        {
            Handbraking = true;
        }
        else
        {
            Handbraking = false;
        }

        if(Input.GetKeyDown(KeyCode.R))
        {
            Debug.LogError("Reverse Registered");
            EngageReverse();
        }

        //Measuring and setting the value of CurrentVelo
        currentVelo = CarBody.velocity.magnitude * (3600/1000);
    }


    //This function attempts to puts the Car into reverse gear 
    void EngageReverse()
    {
        //ENGAGES the reverse Gear
        if(currentVelo > gear01.gear_MinVelo && currentVelo <= 5 && Reverse == false)
        {
            Reverse = true;
            currentGear = gearReverse;
            Debug.LogWarning("Reverse Engaged");
            return;
        }

        //DISENGAGES the reverse gear
        else if(Reverse == true)
        {
            Reverse = false;
            currentGear = gear01;
            Debug.LogWarning("Reverse Disengaged");
            return;
        }

        Debug.LogError("Reverse Failed");
    }

    //Funtion to Apply Brakes
    void UpdateBrakes()
    {
        if (Braking == true)
        {
            FrontLeft.brakeTorque = brakeStrength;
            FrontRight.brakeTorque = brakeStrength;
        }
        else
        {
            FrontLeft.brakeTorque = 0;
            FrontRight.brakeTorque = 0;
        }

        if (Handbraking == true)
        {
            RearLeft.brakeTorque = handBrakeStrength;
            RearRight.brakeTorque = handBrakeStrength;
        }
        else
        {
            RearLeft.brakeTorque = 0;
            RearRight.brakeTorque = 0;
        }
    }

    //Function to update the wheel meshes
    void UpdateWheel(WheelCollider WCollider, Transform WTransform)
    {
        // get wheel collider state
        Vector3 WheelPosition;
        Quaternion WheelRotation;

        WCollider.GetWorldPose(out WheelPosition, out WheelRotation);

        //Set wheel transform State
        WTransform.position = WheelPosition;
        WTransform.rotation = WheelRotation;
    }
}